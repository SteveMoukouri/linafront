import { Component, OnInit } from '@angular/core';
import { VoiceRecognitionService } from 'src/app/service/voice-recognition.service';
import { faMicrophoneLines } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  //icons
  faMicrophoneLines = faMicrophoneLines;

  phrase:string ="";

  text: string;
  speak:boolean = false;


  constructor(public voiceService : VoiceRecognitionService) {
    this.voiceService.init()
   }

  ngOnInit(): void {
  }

  startListen(){
    this.speak = !this.speak;   
    this.speak ? this.voiceService.start() : this.voiceService.stop()
  }

  search(){
    console.log(this.phrase)
  }

}
